/* OTA example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdbool.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_ota_ops.h"

#include "nvs.h"
#include "nvs_flash.h"
#include <libesphttpd/platform.h>
#include <libesphttpd/httpd.h>
#include <libesphttpd/cgiflash.h>
#include <http_server.h>

#define EXAMPLE_WIFI_SSID CONFIG_WIFI_SSID
#define EXAMPLE_WIFI_PASS CONFIG_WIFI_PASSWORD

#define BUFFSIZE 1024
#define TEXT_BUFFSIZE 1024

static const char *TAG = "ota";
/*an ota data write buffer ready to write to the flash*/
static char ota_write_data[BUFFSIZE + 1] = { 0 };
/*an packet receive buffer*/
static char text[BUFFSIZE + 1] = { 0 };
/* an image total length*/
static int binary_file_length = 0;
/*socket id*/
static int socket_id = -1;

/* FreeRTOS event group to signal when we are connected & ready to make a request */
static EventGroupHandle_t wifi_event_group;

/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
const int CONNECTED_BIT = BIT0;

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch (event->event_id) {
    case SYSTEM_EVENT_AP_START:
        xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
        break;
    default:
        break;
    }
    return ESP_OK;
}
static void __attribute__((noreturn)) task_fatal_error()
{
    ESP_LOGE(TAG, "Exiting task due to fatal error...");
    close(socket_id);
    (void)vTaskDelete(NULL);

    while (1) {
        ;
    }
}


static void initialise_wifi(void)
{
    tcpip_adapter_init();
    wifi_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK( esp_event_loop_init(event_handler, NULL) );
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
    wifi_config_t wifi_config = {
                                 .ap = {
                                        .ssid = EXAMPLE_WIFI_SSID,
                                        .password = EXAMPLE_WIFI_PASS,
                                        .ssid_len = strlen(EXAMPLE_WIFI_SSID),
                                        .max_connection = 3,
                                        .authmode = WIFI_AUTH_WPA_WPA2_PSK
                                        },
    };
    ESP_LOGI(TAG, "Setting WiFi configuration SSID %s...", wifi_config.ap.ssid);
    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_AP) );
    ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config) );
    ESP_ERROR_CHECK( esp_wifi_start() );
}
static const char uploadPage[] = "<form action=\"upload\" method=\"post\" enctype=\"multipart/form-data\">"
    "Select firmware file to upload:"
    "<input type=\"file\" name=\"fileToUpload\" id=\"fileToUpload\">"
    "<input type=\"submit\" value=\"Upload Image\" name=\"submit\">"
    "</form>";

static void getHandler(http_context_t http_ctx, void* nothing){
    ESP_LOGD(TAG, "GET handler");
    http_response_begin(http_ctx, 200, "text/plain", 3);
    //http_response_write(http_ctx, &buffer);
    http_response_end(http_ctx);
}

enum POST_STATE{POST_HEADER, POST_DATA,POST_FOOTER};
size_t total;
static void postHandler(http_context_t http_ctx, void*nothing){

    ESP_LOGD(TAG,"POST Handler");
    /*const char* boundary = http_request_get_header(http_ctx, "Content-Type");
    if(boundary != NULL){
        //  ESP_LOGD(TAG, "boundary: %s", boundary+30);
        }*/
    char *data;
    size_t size;
    http_request_get_data(http_ctx,&data, &size);
    total+= size;
    ESP_LOGD(TAG, "%d %d",size, total);

    //ESP_LOGD(TAG, "%.*s", size, data);
}


CgiStatus  cgiGreetUser(HttpdConnData *connData) {
	int len;			//length of user name
	char name[128];		//Temporary buffer for name
	char output[256];	//Temporary buffer for HTML output

	//If the browser unexpectedly closes the connection, the CGI will be called
	//with connData->conn=NULL. We can use this to clean up any data. It's not really
	//used in this simple CGI function.
	if (connData->conn==NULL) {
		//Connection aborted. Clean up.
		return HTTPD_CGI_DONE;
	}

	if (connData->requestType!=HTTPD_METHOD_GET) {
		//Sorry, we only accept GET requests.
		httpdStartResponse(connData, 406);  //http error code 'unacceptable'
		httpdEndHeaders(connData);
		return HTTPD_CGI_DONE;
	}

	//Look for the 'name' GET value. If found, urldecode it and return it into the 'name' var.
	len=httpdFindArg(connData->getArgs, "name", name, sizeof(name));
	if (len==-1) {
		//If the result of httpdFindArg is -1, the variable isn't found in the data.
		strcpy(name, "unknown person");
	} else {
		//If len isn't -1, the variable is found and is copied to the 'name' variable
	}

	//Generate the header
	//We want the header to start with HTTP code 200, which means the document is found.
	httpdStartResponse(connData, 200);
	//We are going to send some HTML.
	httpdHeader(connData, "Content-Type", "text/html");
	//No more headers.
	httpdEndHeaders(connData);

	//We're going to send the HTML as two pieces: a head and a body. We could've also done
	//it in one go, but this demonstrates multiple ways of calling httpdSend.
	//Send the HTML head. Using -1 as the length will make httpdSend take the length
	//of the zero-terminated string it's passed as the amount of data to send.
	//httpdSend(connData, "<html><head><title>Page</title></head>", -1);
	//Generate the HTML body.
    httpdSend(connData, uploadPage, sizeof(uploadPage));
	//Send HTML body to webbrowser. We use the length as calculated by sprintf here.
	//Using -1 again would also have worked, but this is more efficient.
	//httpdSend(connData, output, len);

	//All done.
	return HTTPD_CGI_DONE;
}

CgiUploadFlashDef flashdef={
                            .type=CGIFLASH_TYPE_FW,
                            .fw1Pos=0xc0000,
	.fw2Pos=0x260000,
	.fwSize=0x1a0000,
                            .tagName="ota"
};
static void ota_example_task(void *pvParameter)
{
    esp_err_t err;
    /* update handle : set by esp_ota_begin(), must be freed via esp_ota_end() */
    esp_ota_handle_t update_handle = 0 ;
    const esp_partition_t *update_partition = NULL;

    ESP_LOGI(TAG, "Starting OTA example...");

    const esp_partition_t *configured = esp_ota_get_boot_partition();
    const esp_partition_t *running = esp_ota_get_running_partition();

    if (configured != running) {
        ESP_LOGW(TAG, "Configured OTA boot partition at offset 0x%08x, but running from offset 0x%08x",
                 configured->address, running->address);
        ESP_LOGW(TAG, "(This can happen if either the OTA boot data or preferred boot image become corrupted somehow.)");
    }
    ESP_LOGI(TAG, "Running partition type %d subtype %d (offset 0x%08x)",
             running->type, running->subtype, running->address);

    /* Wait for the callback to set the CONNECTED_BIT in the
       event group.
    */
    xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT,
                        false, true, portMAX_DELAY);
    ESP_LOGI(TAG, "AP is up; starting server");

    HttpdBuiltInUrl builtInUrls[]={
	{"/", cgiGreetUser, "/index.cgi"},
    {"/upload", cgiUploadFirmware, &flashdef},
	{NULL, NULL, NULL}
    };
    httpdInit(builtInUrls, 80,0);


    /*    http_server_options_t httpOpts = HTTP_SERVER_OPTIONS_DEFAULT();
    http_server_t server;
    http_server_start(&httpOpts, &server);

    http_register_handler(server, "/", HTTP_GET, HTTP_HANDLE_URI, getHandler, NULL);
    http_register_handler(server, "/upload", HTTP_POST, HTTP_HANDLE_DATA, postHandler, NULL);
    */
    update_partition = esp_ota_get_next_update_partition(NULL);
    ESP_LOGI(TAG, "Writing to partition subtype %d at offset 0x%x",
             update_partition->subtype, update_partition->address);
             assert(update_partition != NULL);

    /*err = esp_ota_begin(update_partition, OTA_SIZE_UNKNOWN, &update_handle);
      if (err != ESP_OK) {
      ESP_LOGE(TAG, "esp_ota_begin failed (%s)", esp_err_to_name(err));
      task_fatal_error();
      }
      ESP_LOGI(TAG, "esp_ota_begin succeeded");

      ESP_LOGI(TAG, "Total Write binary data length : %d", binary_file_length);

      if (esp_ota_end(update_handle) != ESP_OK) {
      ESP_LOGE(TAG, "esp_ota_end failed!");
      task_fatal_error();
      }
      err = esp_ota_set_boot_partition(update_partition);
      if (err != ESP_OK) {
      ESP_LOGE(TAG, "esp_ota_set_boot_partition failed (%s)!", esp_err_to_name(err));
      task_fatal_error();
      }
      ESP_LOGI(TAG, "Prepare to restart system!");
      esp_restart();*/
    for(;;){
        vTaskDelay(100);
    }
    return ;
}

void app_main()
{
    // Initialize NVS.
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
        // OTA app partition table has a smaller NVS partition size than the non-OTA
        // partition table. This size mismatch may cause NVS initialization to fail.
        // If this happens, we erase NVS partition and initialize NVS again.
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );

    initialise_wifi();
    xTaskCreate(&ota_example_task, "ota_example_task", 8192, NULL, 5, NULL);
}
