
// Show_nvs_keys.ino
// Read all the keys from nvs partition and dump this information.
// 04-dec-2017, Ed Smallenburg.
// 25-FEB-2018, @stickbreaker
//
#include <stdio.h>
#include <string.h>
#include <nvs.h>
#include <nvs_flash.h>

#include <esp_partition.h>
// Debug buffer size

/* from nvs_types.hpp
   enum class ItemType : uint8_t {
   U8   = 0x01,
   I8   = 0x11,
   U16  = 0x02,
   I16  = 0x12,
   U32  = 0x04,
   I32  = 0x14,
   U64  = 0x08,
   I64  = 0x18,
   SZ   = 0x21,
   BLOB = 0x41,
   ANY  = 0xff
   };
*/

// From IDF nvs_page.hpp
const uint32_t PSB_INIT = 0x1;
const uint32_t PSB_FULL = 0x2;
const uint32_t PSB_FREEING = 0x4;
const uint32_t PSB_CORRUPT = 0x8;

const uint32_t ESB_WRITTEN = 0x1;
const uint32_t ESB_ERASED = 0x2;

const uint32_t SEC_SIZE = SPI_FLASH_SEC_SIZE;

const size_t ENTRY_SIZE  = 32;
const size_t ENTRY_COUNT = 126;
const uint32_t INVALID_ENTRY = 0xffffffff;

const size_t BLOB_MAX_SIZE = ENTRY_SIZE * (ENTRY_COUNT / 2 - 1);

const uint8_t NS_INDEX = 0;
const uint8_t NS_ANY = 255;


// All bits set, default state after flash erase. Page has not been initialized yet.
const uint32_t UNINITIALIZED = 0xffffffff;

// Page is initialized, and will accept writes.
const uint32_t ACTIVE        = UNINITIALIZED & ~PSB_INIT;

// Page is marked as full and will not accept new writes.
const uint32_t FULL          = ACTIVE & ~PSB_FULL;

// Data is being moved from this page to a new one.
const uint32_t  FREEING       = FULL & ~PSB_FREEING;

// Page was found to be in a corrupt and unrecoverable state.
// Instead of being erased immediately, it will be kept for diagnostics and data recovery.
// It will be erased once we run out out free pages.
const uint32_t  CORRUPT       = FREEING & ~PSB_CORRUPT;

// Page object wasn't loaded from flash memory
const uint32_t  INVALID       = 0;

// end from nvs_page.hpp

struct NVS_DATA{
  union{
    struct {
      uint16_t size;
      uint16_t rsv;
      uint32_t CRC;
    };
    struct {
      uint8_t data_u8;
      uint8_t f0[7];
    };
    struct {
      int8_t data_i8;
      uint8_t f1[7];
    };
    struct {
      uint16_t data_u16;
      uint8_t f2[6];
    };
    struct {
      int16_t data_i16;
      uint8_t f3[6];
    };
    struct {
      uint32_t data_u32;
      uint8_t f4[4];
    };
    struct {
      int32_t data_i32;
      uint8_t f5[4];
    };
    struct {
      uint64_t data_u64;
    };
    struct {
      int64_t data_i64;
    };
  };
};

struct NVS_ENTRY {
  uint8_t  Ns;         // Namespace ID
  uint8_t  Type;       // Type of value
  uint8_t  Span;       // Number of entries used for this item
  uint8_t  Rvs;        // Reserved, should be 0xFF
  uint32_t CRC;        // CRC
  char     Key[16];    // Key in Ascii
  NVS_DATA Data;       // Data in entry
};

struct nvs_page {       // For nvs entries 1 page is 4096
  uint32_t  State;
  uint32_t  Seqnr;
  uint32_t  Unused[5];
  uint32_t  CRC;
  uint8_t   Bitmap[32];
  NVS_ENTRY Entry[126];
};

struct NAME_SPACE {
  uint8_t nsID;
  char    name[16];
};

struct NS_ARRAY{
  uint8_t count;
  NAME_SPACE ns[255];
};


// Common data
nvs_page buf;
uint8_t * pageOrder=NULL; // current sequence for nvs pages pageOrder[0] is first page,
// usage is: offset = pageOrder[page]*sizeof(nvs_page);
NS_ARRAY * NA=NULL;



//
// test page inuse bitmap
//
void testBitMap(const esp_partition_t* nvs){
uint8_t j=0,bm,k=0,l=0,m=0;
char dispBuf[100];
char ch;
memset(dispBuf,' ',100);
while(j<126){
  bm =(buf.Bitmap[j/4] >>((j%4)*2)) & 0x03;  // Get bitmap for this entry
  switch(bm){
    case 0: // erased (deleted)
      ch='D';
      l=0;
      break;
    case 1: // undefined
      ch='u';
      l=0;
      m = buf.Entry[j].Ns;
      break;
    case 2: // in use, WRITTEN
      ch='*';
      if(l==0){
 //       Serial.printf("%3d: %15s %02x, %d\n",j,buf.Entry[j].Key,
 //         buf.Entry[j].Type,buf.Entry[j].Span);
        l=buf.Entry[j].Span;
        m= buf.Entry[j].Ns;
        }
      l--;
      break;
    case 3: // empty,
      ch='.';
      l=0;
      m = buf.Entry[j].Ns;
      break;
    default : ;
    }
  dispBuf[(j%16)+49]=ch;
  dispBuf[(j%16)+50]='\0';
  if(ch=='D'){
    dispBuf[k]=' ';
    k+=3;
    }
  else k += sprintf((char*)&dispBuf[k]," %02X",m);

  j++;

  if((j%16)==0){
    dispBuf[k]=' ';
    //Serial.printf("%3d:%s\n",((j/16)-1)*16,dispBuf);
    memset(dispBuf,' ',100);
    k=0;
    }
  }
if((j%16)!=0){
  dispBuf[k]=' ';
  //Serial.printf("%3d:%s\n",(j/16)*16,dispBuf);
  memset(dispBuf,' ',100);
  k=0;
  }
}

//
// initialize logical page translation table, NameSpace list.
//
void refreshNVS(const esp_partition_t* nvs ){
if(NA) free(NA);
NA =(NS_ARRAY*)malloc(1);
NA->count=0;
if(pageOrder) free(pageOrder);
uint8_t i = 0;
uint16_t offset = 0;
esp_err_t result = ESP_OK;
i = nvs->size / sizeof(nvs_page);
pageOrder = (uint8_t*)malloc(i);
while(i>0) pageOrder[--i]=255;
i=0;
offset=0;
while(i< (nvs->size / sizeof(nvs_page))){
  offset = i*sizeof(nvs_page);
  result = esp_partition_read( nvs, offset, &buf, sizeof(nvs_page));  //
  if(result == ESP_OK){
    //Serial.printf("page[%u].State=0x%lx ",i,buf.State);
    dispPageState(buf.State);
    if((buf.State == ACTIVE)||(buf.State==FULL)){
      if(buf.Seqnr>((nvs->size / sizeof(nvs_page))-1)){
        //Serial.printf("Sequence value InValid: page[%u].Seqnr=%u ",i,buf.Seqnr);
        }
      else {
        if(pageOrder[buf.Seqnr]!=255){
          //Serial.printf("Sequence error %u = %u, pageOrder[%u]=%u Already inuse!",i,buf.Seqnr,buf.Seqnr,pageOrder[
           buf.Seqnr]);
          }
        else {
          pageOrder[buf.Seqnr] = i;
          //Serial.printf(" Seqnr=%u ",buf.Seqnr);
          }
        }
      }
 else {}//Serial.printf(" Page[%u] not active, State = 0x%lx Seqnr=%u ",i,buf.State,buf.Seqnr);
   /*Serial.println("\n Entry bitmap\n"
     "   : Owning NameSpace index (hex)                     Status ('.' empty, '*' inuse, 'D' deleted)");*/

    testBitMap(nvs);
    }
 else {}//Serial.printf(" Page[%u] read Error = 0x%lx",i,result);
//Serial.println();
  i++;
  }

/* manually create pageOrder because My ESP32 NVS is hosed
pageOrder[1]=1; // jam
pageOrder[2]=3;
pageOrder[3]=4;
*/
//Serial.printf("\nLogical Page to Physical Sector\n");
for(i=0;i<(nvs->size / sizeof(nvs_page));i++){
  //Serial.printf("Page %d -> %u\n",i,pageOrder[i]);
  }
// find namespaces
i=0;
while(i<(nvs->size / sizeof(nvs_page))){
  if(pageOrder[i] != 255) {
    offset = pageOrder[i] * sizeof(nvs_page);
    result = esp_partition_read( nvs, offset, &buf, sizeof(nvs_page));  //
    if(result == ESP_OK){
      uint8_t j=0,bm;
      while(j<126){
        bm =(buf.Bitmap[j/4] >>((j%4)*2)) & 0x03;  // Get bitmap for this entry
        if((bm == 2) &&(buf.Entry[j].Ns == 0)){ // found namespace entry
        // add to NA
          NA = (NS_ARRAY*)realloc(NA,(((++NA->count)*sizeof(NAME_SPACE))+1));
          NA->ns[NA->count-1].nsID= buf.Entry[j].Data.data_u8;
          strcpy(NA->ns[NA->count-1].name, buf.Entry[j].Key);
          }
        if(bm==2) {
          j += buf.Entry[j].Span;                             // Next entry
          }
        else {
          j++;
          }
        }
      }
    }
  i++;
  }
}
